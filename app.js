const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const db = require('./config/database');
const flash = require('connect-flash');
const session = require('express-session');
const passport = require('passport');
const multer = require('multer');
const path = require('path');

const app = express();

// CSS
app.use(express.static(path.join(__dirname, 'public')));
app.use('/css', express.static(__dirname + 'public/css'));

// DISPLAY IMAGES FROM PUBLIC FOLDER
app.use(express.static(path.join(__dirname, '/public')));
app.use(express.static(__dirname + '/'));


// DB TEST
db.authenticate()
    .then(() => console.log('DATABASE Connected...'))
    .catch(err => console.log('Error: '+ err))

// EJS engine
app.use(expressLayouts);
app.set('view engine', 'ejs');
// app.set('views', path.join(__dirname, 'views'));


// BODY PARSER
app.use(bodyParser());
app.use(bodyParser({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb'}));
app.use(bodyParser.urlencoded({extended: false}));
// MORGAN
app.use(morgan('dev'))

// EXPRESS SESSION
app.use(session({
    secret: 'secret',
    // resave: false,
    // saveUninitialized: true,
    // cookie: { secure: true }
  }))

// PASSPORT MIDLEWARE
app.use(passport.initialize());
app.use(passport.session()); 
require('./config/passport')(passport);
   

// CONNECT FLASH
app.use(flash());
// FLASH JANE GLOBAL VARS ESKERTU USHIN QOLDANYLADY
// GLOBAL VARS
app.use(function(req, res, next) {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  next();
});

// ROUTES
app.use('/', require('./routes/index'));
app.use('/users', require('./routes/users'));

const PORT = process.env.PORT || 5000;

app.listen(PORT, console.log('SERVER ON 5000'));