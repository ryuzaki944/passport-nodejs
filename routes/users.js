const express = require('express');
const router = express.Router();
const db = require('../models/')
const bcrypt = require('bcryptjs');
const passport = require('passport')


// LOGIN PAGE
router.get('/login', (req, res) => res.render('login'))


// REGISTER PAGE
router.get('/register', (req, res) => res.render('register'))

router.post('/register', async(req, res) => {
        const { email, password } = req.body;
        let errors = [];

        if (!email || !password) {
            errors.push({ msg: 'Please enter all fields' });
        }

        if (password.length < 6) {
            errors.push({ msg: 'Password must be at least 6 characters' });
        }

        if (errors.length > 0) {
            res.render('register', {
                errors,
                email,
                password
            });
        } else {
            // VALIDATION-GA NE ISTERIN BILMIM
            // Validation Pass
            // Users.findOne()
            // .then(users => {
            //   if(users) {
            //     // User Exists
            //     errors.push({ msg: 'Email is already registered' })
            //     res.render('register', {
            //       errors,
            //       email,
            //       password
            //     })
            //   } else {
            const hashedPassword = await bcrypt.hash(req.body.password, 10)
            const data = {
                email: req.body.email,
                password: hashedPassword
            }

            // Insert into table
            db.User.create(data)
                .then(user => res.redirect('/users/login'))
                .catch(err => {
                    console.log(err),
                        res.redirect('/users/register')
                })

            console.log(data)

        };
    })
    //   }
    // })

// Login
router.post('/login', passport.authenticate('local', {
    failureRedirect: '/users/login'
}), (req, res, next) => {
    console.log('tut') //req.user) 
    res.redirect('/dashboard')
});

// Logout
router.get('/logout', (req, res) => {
    req.logout();
    res.redirect('/users/login');
});

module.exports = router;