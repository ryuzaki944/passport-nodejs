const express = require('express');
const router = express.Router();
const db = require('../models/');
const { ensureAuthenticated, forwardAuthenticated } = require('../config/auth')

const multer = require('multer')
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './public/')
    },
    filename: function (req, file, cb) {
      cb(null, file.originalname + '-' + Date.now())
    }
  })
  
const upload = multer({ storage: storage })


// WELCOME PAGE
router.get('/', forwardAuthenticated, (req, res) => {
    res.render('welcome')
});

// DASHBOARD PAGE
router.get('/dashboard', ensureAuthenticated, (req, res) => {
    console.log('dashboard', req.user.id)
    res.render('dashboard', {
        email: req.user.email,
    })
});

// GET LIST OF BLOCKS
router.get('/dashboard/blocks', ensureAuthenticated, (req, res) => {
    //   Blocks.findAll({
    //     include: [{
    //         model:Users,
    //         required: true
    //     }]
    // })
    // .then(resault => {res.json(resault)})
    db.Block.findAll({
        include: [{   
        model: db.User,
        attributes: ['id', 'email']
        }]
        })
        .then(Block => {
            // USER.EMAIL EJS-TA JAZYLADY EKEN
            res.render('blocks', {
                data: Block            
            });
        })
        .catch(err => console.log(err))

    // {
    //   Users.findAll()
    //     .then(gigs => {
    //       console.log(gigs.id);
    //       console.log('here users');
    //       res.render('blocks', {
    //         user: req.body.email
    //       });
    //     })
    //     .catch(err => console.log(err))
    // }
});

// ADD A BLOCK
router.post('/dashboard/blocks/add', upload.single('image'), async (req, res) => {
    const data = {
        // LOGIN ID-DY ENGIZEDI
        user_id: req.user.id,
        block: req.body.block,
        image: req.file.filename
    }
    let { user_id, block, image } = data;

    // INSERT INTO BLOCKS
    db.Block.create({
            user_id,
            block,
            image
        })
        .then(blocks => res.redirect('/dashboard/blocks'),console.log(req.file))
        .catch(err => console.log(err))
})

// PROFILE PAGE
router.get('/dashboard/profile/:id', ensureAuthenticated, (req, res) => {
    db.Block.findAll({
        where:{ user_id: req.params.id },
        include: [{   
        model: db.User,
        attributes: ['id', 'email']
        },{
            model:db.Comments,
            attributes: ['comment']
        }]
        })
        .then(Block => {
            // USER.EMAIL EJS-TA JAZYLADY EKEN
            res.render('profile', {
                data: Block           
            });
        })
        .catch(err => console.log(err))
})

// MY PAGE
router.get('/dashboard/myprofile', ensureAuthenticated, (req, res) => {
    db.Block.findAll({
        where:{ user_id: req.user.id },
        include: [{   
        model: db.User,
        attributes: ['id', 'email']
        },
        {
        model:db.Comments,
        attributes: ['block_id','comment']
        }]
        })
        .then(Block => {
            // USER.EMAIL EJS-TA JAZYLADY EKEN
            res.render('myprofile', {
                data: Block            
            },console.log(db.Comments.block_id));
        })
        .catch(err => console.log(err))    
})


// DELETE A BLOCK
// router.get('/dashboard/blocks/delete/:id', (req, res) => {
//   Blocks.destroy({ where: {id: req.params.id} })
//   .then(() => res.redirect('/dashboard/blocks'), console.log('DELEEETEEEE'))
//   .catch(err => console.log(err))
// })

// // EDIT A BLOCK
// router.get('/dashboard/blocks/edit/:id', (req, res) => {
//   {
//   Blocks.findOne({ where : { id: req.params.id }})
//     .then(blocks => {
//       res.render('edit', {
//         data: blocks,
//       });
//     })
//     .catch(err => console.log(err))
//   }
//   console.log('EDIT ID: ',req.params.id)
// })

// router.post('/dashboard/blocks/edit/:id/add',(req, res) => {
//   console.log('ADD ID EDIT: ',req.params.id)
//   // const data = {
//   //   // LOGIN ID-DY ENGIZEDI
//   //   id : req.body.id,
//   //   block: req.body.block
//   // }
//   // let { id,block } = data;
//   Blocks.update( {block:req.body.block} , {where: {id: req.params.id}}  )
//   .then(() => res.redirect('/dashboard/blocks'), console.log(req.params.id))
//   .catch(err => console.log(err))
// })

// TUIMESHEMEN OSHIRU
// req.params.id
// EDIT - new page, info o blocke

module.exports = router;