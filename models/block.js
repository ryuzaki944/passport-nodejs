'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Block extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            Block.belongsTo(models.User, {
                foreignKey: 'user_id'
            }),
            Block.hasMany(models.Comments, {
                foreignKey: 'id'
            })
        }
    };
    Block.init({
        block: DataTypes.TEXT,
        user_id: DataTypes.INTEGER,
        image:DataTypes.TEXT
    }, {
        sequelize,
        modelName: 'Block',
    });
    return Block;
};