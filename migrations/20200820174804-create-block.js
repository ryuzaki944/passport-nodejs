'use strict';
module.exports = {
    up: async(queryInterface, Sequelize) => {
        await queryInterface.createTable('Blocks', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            block: {
                type: Sequelize.TEXT
            },
            user_id: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'Users', // name of Target model
                    key: 'id', // key in Target model that we're referencing
                }
            },
            image: {
                type: Sequelize.TEXT
            }
        });
    },
    down: async(queryInterface, Sequelize) => {
        await queryInterface.dropTable('Blocks');
    }
};