const { Sequelize } = require('sequelize');

module.exports = new Sequelize('Decode_example', 'postgres', '123456', {
  host: 'localhost',
  dialect:'postgres',
  define:{
    // WAS IST DAS????????
    timestamps: false
  },
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
    },
});