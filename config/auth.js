module.exports = {
    ensureAuthenticated: (req, res, next) => {
      if (req.isAuthenticated()) {
        return next();
      }
      console.log('err auth')
      res.redirect('/users/login');
    },
    forwardAuthenticated: (req, res, next) => {
      console.log(req.isAuthenticated(), req.user)
      if (!req.isAuthenticated()) {
        return next();
      }
      res.redirect('/');     
    }
  };
  // MUMKIN MYNDA NE PASSPORTTA OSHIBKA BAR
  // MYNDA AIAGYNA SCHEIN TUSINBEDIM