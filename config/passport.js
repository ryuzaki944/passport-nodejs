const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcryptjs');

// USER MODEL
const db = require('../models/')

module.exports = (passport) => {
    passport.use(
        new LocalStrategy({ usernameField: 'email' }, (email, password, done) => {
            // Match user
            db.User.findOne({
                    where: { email: email }
                })
                .then(users => {
                    if (!users) {
                        return done(null, false, { message: 'That email is not registered' }, console.log(users));
                    }


                    // Match password
                    bcrypt.compare(password, users.password, (err, isMatch) => {
                        if (err) throw err;

                        if (isMatch) {

                            return done(null, users);
                        } else {
                            return done(null, false, { message: 'Password incorrect' });
                        }
                    });
                })
                .catch(err => console.log(err))
        })
    );

    passport.serializeUser((users, done) => {
        done(null, users.id);
    });

    passport.deserializeUser((id, done) => {
        db.User.findByPk(id).then(users => done(null, users)).catch(err => done(err, null));
    });
};